@extends('layouts.app')
@section('content')
<div class="page-title">
    <h3>{{ $title }}</h3>
    <p class="text-subtitle text-muted"></p>
</div>
<div class="row">
        <div class="pull-right">
        {{-- <form action="{{route('employee-create')}}" method="POST"> --}}
            <button class="btn btn-sm btn-primary" style="float: right;" onclick="showCreate();">Employee</button>
            <button class="btn btn-sm btn-primary" style="word-spacing: 30px;" id="reload">Reload</button>
         <button class="btn btn-sm btn-warning" style="float: right; display: none;" id="hide" onclick="hideCreate();">Hide</button>
        </div>
    {{-- </form> --}}
</div>
<div class="space-bot"></div>
<div id="form"></div>
{{-- inlcude form --}}
<div class="row mb-4" id="form-employee-index">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Forms Employee</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <table class='table table-striped' id="employee-api">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>City</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script type="text/javascript">
    $(document).ready(function(){
        getEmployee();
})

function getEmployee() {
    let t = $('#employee-api').DataTable({
                processing: true,
                serverSide: true,
                scrollY: false,
                // dom: 'ltrip',
                ajax: '{{ url("api/v1/employee") }}',
                columns: [
                    {data: 'id', name: 'id', width: '4%'},
                    {data: 'first_name', name: 'first_name',width: '14%'},
                    {data: 'last_name', name: 'last_name',width: '14%'},
                    {data: 'email', name: 'email',width: '14%'},
                    {data: 'phone', name: 'phone',width: '14%'},
                    {data: 'action', name:'action'},
                ],
                "order": [[ 0, 'desc' ]],
                "pageLength": 10
            });
}
$('#reload').click(function(){
    $('#employee-api').DataTable().ajax.reload();
})

function showCreate() {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "{{ route('employee-create')}}",
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data).show();
                $('#form-employee-index').attr("style", "display:none");
                $('#hide').attr("style", "display:");
            },
        });
    }

    function showEdit(id) {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "{{ url('/web/employee/edit/')}}" + '/' + id,
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data).show();
                $('#form-employee-index').attr("style", "display:none");
                $('#hide').attr("style", "display:");
            },
        });
    }
function hideCreate() {
    $('#form-employee-index').attr("style", "display:");
    $('#hide').attr("style", "display:none");
    $('#form').attr("style", "display:none");
}
</script>
@endpush