    <div id="alert"></div>
    {{-- inlcude form --}}
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Forms Employee</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        @if(!isset($employee))
                    <form id="form-employee">
                        @else
                        <form id="form-employee-id">
                        @endif
                            <div class="form-body">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>First Name</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input type="text" name="first_name" class="form-control" placeholder="First Name"
                                                id="first_name" value="{{ !isset($employee->first_name) ? '' : $employee->first_name }}">
                                                    <div class="form-control-icon">
                                                        <i data-feather="user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Last Name</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input type="text" name="last_name" class="form-control" placeholder="Last Name"
                                                        id="last_name" value="{{ !isset($employee->last_name) ? '' : $employee->last_name }}">
                                                    <div class="form-control-icon">
                                                        <i data-feather="user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Email</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input type="email" name="email" class="form-control" placeholder="Email"
                                                        id="email" value="{{ !isset($employee->email) ? '' : $employee->email }}">
                                                    <div class="form-control-icon">
                                                        <i data-feather="mail"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Location</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                        <select name="location_id" class="form-control select2">
                                                                <option value="1">Example</option>
                                                            </select>
                                                    <div class="form-control-icon">
                                                        <i data-feather="phone"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Mobile</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <input type="number" id="phone" name="phone" class="form-control" placeholder="Mobile" value="{{ !isset($employee->phone) ? '' : $employee->phone }}">
                                                    <div class="form-control-icon">
                                                        <i data-feather="phone"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Job</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group has-icon-left">
                                                <div class="position-relative">
                                                    <select name="job_id" class="form-control  select2">
                                                        <option value="1">Example</option>
                                                    </select>
                                                    <div class="form-control-icon">
                                                        <i data-feather="lock"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-end ">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                        <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    // $(document).ready(function() {
        $('#form-employee').submit(function(event){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('employee-store')}}",
                dataType: "JSON",
                data: $('#form-employee').serialize(),
                success: function(result) {
                    var alert = '<div class="widget-body" id="status">\
                                    <div class="widget-main">\
                                        <p class="alert alert-sm alert-success">\
                                        Berhasil Di simpan\
                                        </p>\
                                    </div>\
                                </div>';
                                    $('#alert').html(alert).show();
                                    $('#first_name').val('');
                                    $('#last_name').val('');
                                    $('#phone').val('');
                                    $('#email').val('');
                },
                error: function(error) {
                    console.log(error);
                    var alert = '<div class="widget-body" id="status">\
                                    <div class="widget-main">\
                                        <p class="alert alert-sm alert-danger">\
                                       Check Kembali Data\
                                        </p>\
                                    </div>\
                                </div>';
                $('#alert').html(alert).show();
                }
            })
        })
    // })
    </script>