<div id="sidebar" class='active'>
        <div class="sidebar-wrapper active">
{{-- <div class="sidebar-header">
    <img src="/assets/images/logo.svg" alt="" srcset="">
</div> --}}
<div class="sidebar-menu">
    <ul class="menu">
            <li class='sidebar-title'>Main Menu</li>
            <li class="sidebar-item active ">
                <a href="index.html" class='sidebar-link'>
                    <i data-feather="home" width="20"></i> 
                    <span>Dashboard</span>
                </a>
                
            </li>
            <li class="sidebar-item  has-sub">
                <a href="#" class='sidebar-link'>
                    <i data-feather="triangle" width="20"></i> 
                    <span>Management Product</span>
                </a>
                
                <ul class="submenu ">
                    <li>
                        <a href="component-alert.html">Product</a>
                    </li>
                </ul>
            </li>
            <li class="sidebar-item  has-sub">
                <a href="#" class='sidebar-link'>
                    <i data-feather="briefcase" width="20"></i> 
                    <span>Management Employee</span>
                </a>
                <ul class="submenu ">
                    <li>
                    <a href="{{ route('employee')}}">Employee</a>
                    </li>
                    <li>
                        <a href="{{ route('employee-create') }}"">Employee Create</a>
                    </li>
                </ul>
            </li>
            <li class='sidebar-title'>Forms &amp; Tables</li>
            <li class="sidebar-item  has-sub">
                <a href="#" class='sidebar-link'>
                    <i data-feather="file-text" width="20"></i> 
                    <span>Management Job</span>
                </a>
                <ul class="submenu ">
                    <li>
                        <a href="form-element-input.html">Input</a>
                    </li>
                    <li>
                </ul>
            </li>

            <li class="sidebar-item  ">
                <a href="form-layout.html" class='sidebar-link'>
                    <i data-feather="layout" width="20"></i> 
                    <span>Management Customer</span>
                </a>
                
            </li>


        
        
        
            <li class='sidebar-title'>Extra UI</li>
        
        
        
            <li class="sidebar-item  has-sub">
                <a href="#" class='sidebar-link'>
                    <i data-feather="user" width="20"></i> 
                    <span>Management Type</span>
                </a>
                
                <ul class="submenu ">
                    
                    <li>
                        <a href="ui-chatbox.html">Type</a>
                    </li>
                    
                </ul>
                
            </li>

            <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i data-feather="user" width="20"></i> 
                        <span>Management Location</span>
                    </a>
                    
                    <ul class="submenu ">
                        
                        <li>
                            <a href="ui-chatbox.html">Locatiob</a>
                        </li>
                        
                    </ul>
                    
                </li>
            <li class='sidebar-title'>Pages</li>
        
            <li class="sidebar-item  has-sub">
                <a href="#" class='sidebar-link'>
                    <i data-feather="user" width="20"></i> 
                    <span>Management Manager</span>
                </a>
                
                <ul class="submenu ">
                    
                    <li>
                        <a href="auth-login.html">Manager</a>
                    </li>
                    
                </ul>
                
            </li>
    </ul>
</div>
<button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
</div>
    </div>