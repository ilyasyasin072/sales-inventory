<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Dashboard - Voler Admin Dashboard</title>
   {{-- Head --}}
   @include('layouts.header')
</head>


<body>
   <div id="app">
      {{-- Side bar --}}
      @include('layouts.sidebar')
      <div id="main">
         {{-- Navbar --}}
         @include('layouts.navbar')
         <div class="main-content container-fluid">
            @yield('content')
            {{-- EndSection --}}
         </div>
         {{-- Footer --}}
      </div>
   </div>
   {{Html::script('/js/jquery-2.2.4.min.js')}}
   {{Html::script('/js/datatable.js')}}
   {{Html::script('assets/js/feather-icons/feather.min.js')}}
   {{Html::script('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}
   {{Html::script('assets/js/app.js')}}
   {{Html::script('assets/vendors/chartjs/Chart.min.js')}}
   {{Html::script('assets/vendors/apexcharts/apexcharts.min.j')}}
   {{Html::script('assets/js/pages/dashboard.js')}}
   {{Html::script('assets/vendors/simple-datatables/simple-datatables.js')}}
   {{Html::script('assets/js/vendors.js')}}
   {{Html::script('assets/js/main.js')}}

   @stack('js')
   @include('layouts.footer')
</body>

</html>