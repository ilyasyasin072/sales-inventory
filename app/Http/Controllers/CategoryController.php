<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.category.index', ['title' => 'Category'], 200)->render();
    }

    public function index_data() 
    {
        $category = Category::CategoryData();
        return datatables()->of($category)
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.category.create', ['title' => 'Category From Create'], 200)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $format = \Request::format();
        $nameCategory = $request->input('name');
        $descCategory = $request->input('desc');
        $category =  new Category();

        $category->name = $nameCategory;
        $category->description = $descCategory;

        if($category->save()) {
            switch ($format) {
                case 'html':
                default:
                $render = redirect()->with('success', 'Category Success');
                    break;
            }
        } else {

            switch ($format) {
                case 'html':
                default:
                $render = redirect()->with('danger', 'Category Failed');
                    break;
            }
        }
        return $render;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $category = Category::GetId($id);

        return view('pages.category.show', ['category' => $category], 200)->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::GetId($id);

        return view('pages.category,edit', ['category' => $category], 299)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nameCategory = $request->get('name');
        $descCategory = $request->get('desc');

        $category = Category::GetId($id);

        $category->name = $nameCategory;
        $category->description = $descCategory;

        $format = \Request::format();
        if($category->save()) {
            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['category', $category], 200);
                    break;
            }
        } else {
            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['category', $category], 500);
                    break;
            } 
        }
    return $render;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::GetId($id);
        $format = \Request::format();

        if($category->delete()) {
            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['success', $category], 200);
                    break;
            }
        } else {
            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['failed', $category], 500);
                    break;
            }
        }
        return $render;
    }
}
