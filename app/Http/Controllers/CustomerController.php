<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.customer.index', ['title' => 'Customer'], 200)->render();
    }

    public function index_data() 
    {
        $customer = Customer::CustomerData();
        return datatables()->of($customer)
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.customer.create', ['title' => 'Customer'], 200)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $firstName = $request->input('first_name');
        $lastName = $request->input('first_name');
        $address = $request->input('address');
        $phone = $request->input('phone');

        $customer = new Customer();

        $customer->first_name =$firstName;
        $customer->last_name = $lastName;
        $customer->address = $address;
        $customer->phone =  $phone;

        $format = \Request::format();


        if($customer->save()) {

            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['customer', $customer], 200);
                    break;
            }

        } else {

            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['customer', $customer], 200);
                    break;
            }
        }

        return $render;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::GetId($id);
        return view('pages.customer.show', ['title' => 'Customer', 'customer' => $customer], 200)->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $customer = Customer::GetId($id);
        return view('pages.customer.edit', ['title' => 'Customer', 'customer' => $customer], 200)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $firstName = $request->input('first_name');
        $lastName = $request->input('first_name');
        $address = $request->input('address');
        $phone = $request->input('phone');

        $customer = Customer::GetId($id);

        $customer->first_name =$firstName;
        $customer->last_name = $lastName;
        $customer->address = $address;
        $customer->phone =  $phone;

        $format = \Request::format();


        if($customer->save()) {

            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['customer', $customer], 200);
                    break;
            }

        } else {

            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['customer', $customer], 200);
                    break;
            }
        }

        return $render;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::GetId($id);
        $format = \Request::format();

        if($customer->delete()) {
            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['success', $customer], 200);
                    break;
            }
        } else {
            switch ($format) {
                case 'html';
                default:
                    $render = response()->json(['success', $customer], 200);
                    break;
            }

        }

        return $render;
    }
}
