<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subTitle = 'Employee';
        return view('pages.employee.index', ['title' =>  $subTitle ])->render();
    }

    public function index_data() {
        $employee = Employee::EmployeeData();
        return datatables()->of($employee)
        ->editColumn('action', function($employee){
            return '<a class="btn btn-xs btn-primary" onclick="showEdit('.$employee->id.')">Update</a>
            <button class="btn btn-xs btn-danger">Delete</button>';
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subTitle = 'Employee From Create';
        return view('pages.employee.form', ['title' =>  $subTitle ])->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $firstName                  = $request->input('first_name');
        $lastName                   = $request->input('last_name');
        $email                      = $request->input('email');
        $phone                      = $request->input('phone');
        $hireDate                   = Carbon::now();
        $jobId                      = $request->input('job_id');
        $locationId                 = $request->input('location_id');
        
        // var_dump($hireDate); die;

        $employee = new Employee();

        $employee->first_name       = $firstName;
        $employee->last_name        = $lastName;
        $employee->email            = $email;
        $employee->phone            = $phone;
        $employee->hired_date        = $hireDate;
        $employee->job_id           = $jobId;
        $employee->location_id      = $locationId;

        $format = \Request::format();
        
        
        if($employee->save()) {
            switch ($format) {
                case 'html':
                    default:
                        $render = response()->json(['employee' => $employee], 200);
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                    default:
                        $render = response()->json(['employee' => $employee], 500);
                    break;
            }
        }

        return $render;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employeeShow = Employee::GetId($id);

        return view('pages.employee.show', ['employee', $employeeShow], 200)->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employeeEdit= Employee::GetId($id);
        // dd($employeeEdit);
        return view('pages.employee.edit', ['employee' => $employeeEdit])->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $firstName                  = $request->input('first_name');
        $lastName                   = $request->input('last_name');
        $email                      = $request->input('email');
        $phone                      = $request->input('phone');
        $hireDate                   = $request->input('hire_date');
        $jobId                      = $request->input('job_id');
        $locationId                 = $request->input('location_id');

        $employee = Employee::GetId($id);

        $employee->first_name       = $firstName;
        $employee->last_name        = $lastName;
        $employee->email            = $email;
        $employee->phone            = $phone;
        $employee->hire_date        = $hireDate;
        $employee->job_id           = $jobId;
        $employee->location_id      = $locationId;

        $format = \Request::format();
        
        
        if($employee->save()) {
            switch ($format) {
                case 'html':
                    default:
                        $render = response()->json(['status' => 'updated to been successfull'], 200);
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                    default:
                        $render = response()->json(['status' => 'updated to been failed'], 500);
                    break;
            }
        }

        return $render;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
