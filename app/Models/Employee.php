<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Job;
use App\Models\Location;
class Employee extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'hired_date',
        'job_id',
        'location_id',
        'created_at',
    ];
    // protected $appends = ['email_employee'];

    protected $hidden = ['updated_at'];

    public function job() {
        return $this->belongsTo(Job::class);
    }

    public function location() {
        return $this->belongsTo(Location::class);
    }

    // public function getEmailEmployeeAttribute()
    // {
    //     return $this->employee->email;
    // }

    public function scopeGetId($query, $id) {
        return $query->where('id', $id)->first();
    }

    public function scopeEmployeeData($query) {
        return $query->select(['*'])->get();
    }
}
