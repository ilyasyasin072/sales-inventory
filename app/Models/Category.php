<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $gurarded = [];

    protected $fillabel = [
        'name',
        'description',
        'created_at'
    ];

    public function scopeCategoryData($query) {
        return $query->select(['*'])->get();
    }

    public function scopeGetId($query, $id) {
        return $query->where('id', $id)->first();
    }
}
