<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillabel = [
        'first_name', 'last_name', 'address', 'phone', 'created_at'
    ];

    public function scopeCustomerData($query)
    {
        return $query->select(['*'])->get();
    }

    public function scopeGetId($query, $id)
    {
        return $query->where('id', $id)->first();
    }
}
