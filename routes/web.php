<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\LocationController;

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

Route::group(['prefix' => 'web'], function() {
    Route::group(['prefix' => 'employee'], function() {
        Route::get('/', [EmployeeController::class, 'index'])->name('employee');
        Route::get('/create', [EmployeeController::class, 'create'])->name('employee-create');
        Route::get('/edit/{id}', [EmployeeController::class, 'edit'])->name('employee-edit');
        Route::post('/store', [EmployeeController::class, 'store'])->name('employee-store');
    });

    Route::group(['prefix' => 'job'], function(){
        Route::post('/store', [JobController::class, 'store'])->name('job-store');
    });
    Route::group(['prefix' => 'location'], function(){
        Route::post('/store', [LocationController::class, 'store'])->name('location-store');
    });
});

Route::get('/dashboard', function(){
    return view('layouts.apps');
});